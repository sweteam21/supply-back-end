import http.server
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import urllib.parse
import requests
from utils import HTTPRequestUtils
import hashlib
import os
import binascii

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    # POST requests
    def do_POST(self):
        # get path of request
        path = self.path

        # User Login
        # check path of request
        if path == "/api/backend":

            # get all headers
            print('Headers:"', self.headers, '"')
            # or identify what headers you want to get
            print('Content-Type:', self.headers['content-type'])

            # to read body of request
            # 1) grab the length of the body & read that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # print whole body for debugging if you want
            # body is in bytes
            print('Body:', body)

            # bytes to string using .decode()
            print("Body (String):", body.decode())

            # convert body from string to dictionary using loads
            dictionary = json.loads(body)
            print("dictionary (length=" + str(len(dictionary)) + "): " + str(dictionary))

            # Logging i
            if dictionary['flag'] == 'login':

                print('Username:', dictionary['username'])
                print(dictionary['password'])
                print(HTTPRequestUtils.returnUsersPassword(self, dictionary['username']))

                # testingHash = verifyPassword(HTTPRequestUtils.returnUsersPassword(self, dictionary['username']), dictionary['password'])

                # check passwords match (entered plaintext vs stored hash)
                # if verifyPassword(HTTPRequestUtils.returnUsersPassword(self, dictionary['username']), dictionary['password']):
                if dictionary['password'] == HTTPRequestUtils.returnUsersPassword(self, dictionary['username']):
                    self.send_response(200)
                    # done sending headers
                    self.end_headers()
                    # format your json in a string
                    res = '200'
                    # string to bytes
                    bytesStr = res.encode('utf-8')
                    self.wfile.write(bytesStr)


            # !!! FIX SOON !!!     Registering User (Fleet Manager)
            elif dictionary['flag'] == 'register':
                firstName = dictionary['firstName']
                lastName = dictionary['lastName']
                email = dictionary['email']
                username = dictionary['username']
                hashedPassword = dictionary['password'] # hashPassword(dictionary['password'])
                HTTPRequestUtils.insertNewUser(self, firstName, lastName, email, username, hashedPassword)

                # fix!!
                self.send_response(200)
                # done sending headers
                self.end_headers()
                # format your json in a string
                res = '200'
                # string to bytes
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)

            else:
                order_contents = dictionary['contents']
                order_address = dictionary['address']
                order_type = dictionary['tos']
                user_id = dictionary['user_id']
                HTTPRequestUtils.createOrder(self, order_contents, user_id, order_address, order_type)

                self.send_response(200)
                # done sending headers
                self.end_headers()
                # format your json in a string
                res = '200'
                # string to bytes
                bytesStr = res.encode('utf-8')
                self.wfile.write(bytesStr)



    # GET requests
    #will be completed when necessary, currently only using POST
    def do_GET(self):
        # get path of request
        path = self


def main():
    # define port servers will run on
    port = 4021
    # create http server using class & port defined
    httpServer = http.server.HTTPServer(('localhost', port), SimpleHTTPRequestHandler)
    print("Running on port ", port)

    # CHECK ABOUT HOW TO BLOCK W/O BLOCKING OTHER TERMINAL COMMANDS
    httpServer.serve_forever()


if __name__ == "__main__":
    main()
