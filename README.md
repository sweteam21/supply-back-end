**WeGo Supply Side Back End**
*Reed Gauthier, Kathryn Reck, Sergiy Ensary, Summer Klimko, Kyle Flett*

**About**
This repository contains the Python files used by the supply side back end. 
This contains what the supply side common services pages will be using for database requests, API calls, etc.

**How to use this repository**
Before using these files, ensure that mysql and pymsql are installed. team21-supply-HTTPRequestHandler.py handles HTTP requests, and uses helper methods from utils.py.

**Next Steps**
The next step for this repository is to ensure the connection to the demand side is working.